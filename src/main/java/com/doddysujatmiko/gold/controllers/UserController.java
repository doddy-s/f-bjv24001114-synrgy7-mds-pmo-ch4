package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.User;
import com.doddysujatmiko.gold.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("")
    public ResponseEntity<Map> postUser(@RequestBody User user) {
        Map response = userService.create(user);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @GetMapping("/{userId}")
    public ResponseEntity<Map> getUserById(@PathVariable UUID userId) {
        Map response = userService.read(userId);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @PatchMapping("")
    public ResponseEntity<Map> patchUser(@RequestBody User user) {
        Map response = userService.update(user);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Map> deleteUser(@PathVariable UUID userId) {
        Map response = userService.delete(userId);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }
}
