package com.doddysujatmiko.gold.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class Hello {

    @GetMapping("/hello")
    public String hello() {
        return "hllow";
    }

    @GetMapping("/hello/{ur}")
    public String hello_name(@PathVariable String ur) {
        return "hllow" + ur;
    }
}
