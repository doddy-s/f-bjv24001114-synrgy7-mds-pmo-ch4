package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.Product;
import com.doddysujatmiko.gold.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping("")
    public ResponseEntity<Map> postProduct(@RequestBody Product product) {
        Map response = productService.create(product);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @GetMapping("/{productId}")
    public ResponseEntity<Map> getProductById(@PathVariable UUID productId) {
        Map response = productService.read(productId);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @PatchMapping("")
    public ResponseEntity<Map> patchProduct(@RequestBody Product product) {
        Map response = productService.update(product);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<Map> deleteProduct(@PathVariable UUID productId) {
        Map response = productService.delete(productId);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @GetMapping("/page/{page}")
    public ResponseEntity<Map> getProductPage(@PathVariable int page) {
        Map response = productService.readPage(page, 10);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

}
