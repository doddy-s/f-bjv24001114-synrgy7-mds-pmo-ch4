package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.Merchant;
import com.doddysujatmiko.gold.services.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/merchant")
public class MerchantController {
    @Autowired
    private MerchantService merchantService;

    @PostMapping("")
    public ResponseEntity<Map> postMerchant(@RequestBody Merchant merchant) {
        Map response = merchantService.create(merchant);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @GetMapping("/{merchantId}")
    public ResponseEntity<Map> getMerchantById(@PathVariable UUID merchantId) {
        Map response = merchantService.read(merchantId);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @PatchMapping("")
    public ResponseEntity<Map> patchMerchant(@RequestBody Merchant merchant) {
        Map response = merchantService.update(merchant);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @DeleteMapping("/{merchantId}")
    public ResponseEntity<Map> deleteMerchant(@PathVariable UUID merchantId) {
        Map response = merchantService.delete(merchantId);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }
}
