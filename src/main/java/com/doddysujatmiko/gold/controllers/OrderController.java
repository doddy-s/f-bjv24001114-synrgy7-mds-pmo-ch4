package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.Order;
import com.doddysujatmiko.gold.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping("")
    public ResponseEntity<Map> postOrder(@RequestBody Order order) {
        Map response = orderService.create(order);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<Map> getOrderById(@PathVariable UUID orderId) {
        Map response = orderService.read(orderId);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @PatchMapping("")
    public ResponseEntity<Map> patchOrder(@RequestBody Order order) {
        Map response = orderService.update(order);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<Map> deleteOrder(@PathVariable UUID orderId) {
        Map response = orderService.delete(orderId);
        return new ResponseEntity<>(response, HttpStatusCode.valueOf((int) response.get("status")));
    }
}
