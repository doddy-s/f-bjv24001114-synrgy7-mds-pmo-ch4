package com.doddysujatmiko.gold.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Entity(name = "merchants")
@Getter
@Setter
public class Merchant extends AbstractEntity implements Serializable {
    @Column(name = "name")
    private String name;

    @Column(name = "location")
    private String location;

    @Column(name = "open")
    private Boolean open;

    @JsonIgnore
    @OneToMany(mappedBy = "merchant", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Product> products;
}
