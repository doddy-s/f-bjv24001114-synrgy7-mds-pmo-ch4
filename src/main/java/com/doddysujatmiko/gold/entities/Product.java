package com.doddysujatmiko.gold.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Entity(name = "products")
@Getter
@Setter
public class Product extends AbstractEntity implements Serializable {
    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private int price;

    @JsonIgnore
    @ManyToOne(targetEntity = Merchant.class, cascade = CascadeType.ALL)
    @JoinColumn(name="merchant_id", referencedColumnName = "id")
    private Merchant merchant;

    @JsonIgnore
    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<OrderDetail> orderDetails;
}
