package com.doddysujatmiko.gold.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Entity(name = "orders")
@Getter
@Setter
public class Order extends AbstractEntity implements Serializable {
    @Column(name = "destination_address")
    private String destinationAddress;

    @Column(name = "completed")
    private Boolean completed;

    @JsonIgnore
    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(name="user_id", referencedColumnName = "id")
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<OrderDetail> orderDetails;
}
