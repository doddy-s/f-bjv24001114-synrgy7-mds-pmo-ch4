package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.User;

import java.util.Map;
import java.util.UUID;

public interface UserService {
    Map create(User user);
    Map read(UUID id);
    Map update(User user);
    Map delete(UUID id);
}
