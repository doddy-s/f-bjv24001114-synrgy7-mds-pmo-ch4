package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.Order;

import java.util.Map;
import java.util.UUID;

public interface OrderService {
    Map create(Order order);
    Map read(UUID id);
    Map update(Order order);
    Map delete(UUID id);
}
