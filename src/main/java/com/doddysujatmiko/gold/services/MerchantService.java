package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.Merchant;

import java.util.Map;
import java.util.UUID;

public interface MerchantService {
    Map create(Merchant merchant);
    Map read(UUID id);
    Map update(Merchant merchant);
    Map delete(UUID id);
}
