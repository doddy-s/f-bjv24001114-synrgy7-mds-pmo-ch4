package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.Product;

import java.util.Map;
import java.util.UUID;

public interface ProductService {
    Map create(Product product);
    Map read(UUID id);
    Map update(Product product);
    Map delete(UUID id);

    Map readPage(int page, int size);
}
