package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.Merchant;
import com.doddysujatmiko.gold.repositories.MerchantRepository;
import com.doddysujatmiko.gold.services.MerchantService;
import com.doddysujatmiko.gold.utils.Response;
import io.micrometer.common.util.StringUtils;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class MerchantServiceImpl implements MerchantService {

    @Autowired
    private MerchantRepository merchantRepository;

    @Override
    public Map create(Merchant merchant) {
        try {
            if(StringUtils.isEmpty(merchant.getName())) {
                throw new NullPointerException("Name can not be empty");
            }

            if(StringUtils.isEmpty(merchant.getLocation())) {
                throw new NullPointerException("Location can not be empty");
            }

            if(merchant.getOpen() == null) {
                throw new NullPointerException("Open can not be empty");
            }

            return Response.constructSuccess(merchantRepository.save(merchant), 201, "Merchant successfully created");

        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map read(UUID id) {
        Optional<Merchant> merchant = merchantRepository.findById(id);

        if(merchant.isPresent()) {
            return Response.constructSuccess(merchant.get());
        }

        return Response.constructError("Id not found", 404);
    }

    @Override
    public Map update(Merchant merchant) {
        try {
            if(merchant.getId() == null) {
                throw new NullPointerException("Merchant Id can not be empty");
            }

            Optional<Merchant> exMerchant = merchantRepository.findById(merchant.getId());

            if(exMerchant.isEmpty()) {
                throw new EntityNotFoundException("Merchant not found");
            }

            return Response.constructSuccess(merchantRepository.save(merchant), 201, "Merchant successfully updated");
        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            if(e instanceof EntityNotFoundException) {
                return Response.constructError(e.getMessage(), 404);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map delete(UUID id) {
        try {
            if(id == null) {
                throw new NullPointerException("Merchant Id can not be empty");
            }

            Optional<Merchant> exMerchant = merchantRepository.findById(id);

            if(exMerchant.isEmpty()) {
                throw new EntityNotFoundException("Merchant not found");
            }

            merchantRepository.deleteById(id);

            return Response.constructSuccess("", 201, "Merchant successfully deleted");
        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            if(e instanceof EntityNotFoundException) {
                return Response.constructError(e.getMessage(), 404);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }
}
