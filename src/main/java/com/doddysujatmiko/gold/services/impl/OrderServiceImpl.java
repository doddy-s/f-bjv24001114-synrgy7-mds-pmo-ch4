package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.Order;
import com.doddysujatmiko.gold.repositories.OrderRepository;
import com.doddysujatmiko.gold.services.OrderService;
import com.doddysujatmiko.gold.utils.Response;
import io.micrometer.common.util.StringUtils;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService {

//    public static final Logger LOGGER = Logger.

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Map create(Order order) {
        try {
            if(StringUtils.isEmpty(order.getDestinationAddress())) {
                throw new NullPointerException("Ordername can not be empty");
            }

            if(order.getCompleted() == null) {
                throw new NullPointerException("Completed can not be empty");
            }

            return Response.constructSuccess(orderRepository.save(order), 201, "Order successfully created");

        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map read(UUID id) {
        Optional<Order> order = orderRepository.findById(id);

        if(order.isPresent()) {
            return Response.constructSuccess(order.get());
        }

        return Response.constructError("Id not found", 404);
    }

    @Override
    public Map update(Order order) {
        try {
            if(order.getId() == null) {
                throw new NullPointerException("Order Id can not be empty");
            }

            Optional<Order> exOrder = orderRepository.findById(order.getId());

            if(exOrder.isEmpty()) {
                throw new EntityNotFoundException("Order not found");
            }

            return Response.constructSuccess(orderRepository.save(order), 201, "Order successfully updated");
        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            if(e instanceof EntityNotFoundException) {
                return Response.constructError(e.getMessage(), 404);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map delete(UUID id) {
        try {
            if(id == null) {
                throw new NullPointerException("Order Id can not be empty");
            }

            Optional<Order> exOrder = orderRepository.findById(id);

            if(exOrder.isEmpty()) {
                throw new EntityNotFoundException("Order not found");
            }

            orderRepository.deleteById(id);

            return Response.constructSuccess("", 201, "Order successfully deleted");
        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            if(e instanceof EntityNotFoundException) {
                return Response.constructError(e.getMessage(), 404);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }
}
