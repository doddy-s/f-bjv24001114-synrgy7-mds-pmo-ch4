package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.Merchant;
import com.doddysujatmiko.gold.entities.Product;
import com.doddysujatmiko.gold.repositories.ProductRepository;
import com.doddysujatmiko.gold.services.ProductService;
import com.doddysujatmiko.gold.utils.Response;
import io.micrometer.common.util.StringUtils;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Map create(Product product) {
        try {
            if(StringUtils.isEmpty(product.getName())) {
                throw new NullPointerException("Name can not be empty");
            }

//            if(product.getPrice() == null) {
//                throw new NullPointerException("Price can not be empty");
//            }

            return Response.constructSuccess(productRepository.save(product), 201, "Product successfully created");

        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map read(UUID id) {
        Optional<Product> product = productRepository.findById(id);

        if(product.isPresent()) {
            return Response.constructSuccess(product.get());
        }

        return Response.constructError("Id not found", 404);
    }

    @Override
    public Map update(Product product) {
        try {
            if(product.getId() == null) {
                throw new NullPointerException("Product Id can not be empty");
            }

            Optional<Product> exProduct = productRepository.findById(product.getId());

            if(exProduct.isEmpty()) {
                throw new EntityNotFoundException("Product not found");
            }

            return Response.constructSuccess(productRepository.save(product), 201, "Product successfully updated");
        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            if(e instanceof EntityNotFoundException) {
                return Response.constructError(e.getMessage(), 404);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map delete(UUID id) {
        try {
            if(id == null) {
                throw new NullPointerException("Product Id can not be empty");
            }

            Optional<Product> exProduct = productRepository.findById(id);

            if(exProduct.isEmpty()) {
                throw new EntityNotFoundException("Product not found");
            }

            productRepository.deleteById(id);

            return Response.constructSuccess("", 201, "Product successfully deleted");
        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            if(e instanceof EntityNotFoundException) {
                return Response.constructError(e.getMessage(), 404);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map readPage(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Product> productPage = productRepository.findAll(pageable);
        return Response.constructSuccess(productPage);
    }
}
