package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.User;
import com.doddysujatmiko.gold.repositories.UserRepository;
import com.doddysujatmiko.gold.services.UserService;
import com.doddysujatmiko.gold.utils.Response;
import io.micrometer.common.util.StringUtils;
import jakarta.persistence.EntityNotFoundException;
import org.hibernate.FetchNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

//    public static final Logger LOGGER = Logger.

    @Autowired
    private UserRepository userRepository;

    @Override
    public Map create(User user) {
        try {
            if(StringUtils.isEmpty(user.getUsername())) {
                throw new NullPointerException("Username can not be empty");
            }

            if(StringUtils.isEmpty(user.getEmail())) {
                throw new NullPointerException("Email can not be empty");
            }

            if(StringUtils.isEmpty(user.getPassword())) {
                throw new NullPointerException("Password can not be empty");
            }

            return Response.constructSuccess(userRepository.create_user(user.getUsername(), user.getEmail(), user.getPassword()), 201, "User successfully created");

        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map read(UUID id) {
        Optional<User> user = userRepository.findById(id);

        if(user.isPresent()) {
            return Response.constructSuccess(user.get());
        }

        return Response.constructError("Id not found", 404);
    }

    @Override
    public Map update(User user) {
        try {
            if(user.getId() == null) {
                throw new NullPointerException("User Id can not be empty");
            }

            Optional<User> exUser = userRepository.findById(user.getId());

            if(exUser.isEmpty()) {
                throw new EntityNotFoundException("User not found");
            }

            return Response.constructSuccess(userRepository.save(user), 201, "User successfully updated");
        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            if(e instanceof EntityNotFoundException) {
                return Response.constructError(e.getMessage(), 404);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }

    @Override
    public Map delete(UUID id) {
        try {
            if(id == null) {
                throw new NullPointerException("User Id can not be empty");
            }

            Optional<User> exUser = userRepository.findById(id);

            if(exUser.isEmpty()) {
                throw new EntityNotFoundException("User not found");
            }

            userRepository.deleteById(id);

            return Response.constructSuccess("", 201, "User successfully deleted");
        } catch (Throwable e) {
            if(e instanceof NullPointerException) {
                return Response.constructError(e.getMessage(), 400);
            }

            if(e instanceof EntityNotFoundException) {
                return Response.constructError(e.getMessage(), 404);
            }

            return Response.constructError(e.getMessage(), 500);
        }
    }
}
