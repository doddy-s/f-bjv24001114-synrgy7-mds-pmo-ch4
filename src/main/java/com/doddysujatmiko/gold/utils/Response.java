package com.doddysujatmiko.gold.utils;

import com.doddysujatmiko.gold.entities.User;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Response {

    public static Map constructSuccess(Object obj, int code, String message){
        Map map = new HashMap();
        map.put("data", obj);
        map.put("status", code);
        map.put("message", message);
        return map;
    }

    public static Map constructError(Object obj, int code){
        Map map = new HashMap();
        map.put("status", code);
        map.put("message", obj);
        return map;
    }

    public static Map constructSuccess(Object obj) {
        Map map = new HashMap();
        map.put("data", obj);
        map.put("status", 200);
        map.put("message", "success");
        return map;
    }
}