package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

//@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserServiceTest {

    @Autowired
    public UserService userService;

    @Test
    public void saveEmployee(){
        User user = new User();
        user.setUsername("Aldi");
        user.setEmail("e@e.com");
        user.setPassword("Passwd");

        Map map = userService.create(user);
        int responseCode = (Integer) map.get("status");
        Assertions.assertEquals(200, responseCode);
    }


    @Test
    public void getPagination(){
        Map map =  userService.getPage("e@e.com",0,10);
        int responseCode = (Integer) map.get("status");
        Assertions.assertEquals(200, responseCode);
    }


}